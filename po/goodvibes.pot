# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the goodvibes package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: goodvibes\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-07-27 13:35+0700\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: data/com.elboulangero.Goodvibes.appdata.xml.in:4
#: data/com.elboulangero.Goodvibes.desktop.in:3 src/main.c:149
msgid "Goodvibes"
msgstr ""

#: data/com.elboulangero.Goodvibes.appdata.xml.in:5
#: data/com.elboulangero.Goodvibes.desktop.in:5
msgid "Play web radios"
msgstr ""

#: data/com.elboulangero.Goodvibes.appdata.xml.in:10
msgid "Goodvibes is a simple internet radio player for GNU/Linux."
msgstr ""

#: data/com.elboulangero.Goodvibes.appdata.xml.in:13
msgid ""
"It comes with every basic features you can expect from an audio player, such "
"as multimedia keys, notifications, system sleep inhibition, and MPRIS2 "
"support."
msgstr ""

#: data/com.elboulangero.Goodvibes.desktop.in:4
msgid "Radio Player"
msgstr ""

#. TRANSLATORS: Search terms to find this application. Do NOT translate or localize the semicolons! The list MUST also end with a semicolon!
#: data/com.elboulangero.Goodvibes.desktop.in:7
msgid "Audio;Radio;Player;"
msgstr ""

#. TRANSLATORS: Do NOT translate or transliterate this text (this is an icon file name)!
#: data/com.elboulangero.Goodvibes.desktop.in:11
msgid "goodvibes"
msgstr ""

#: data/ui/app-menu.glade:6 data/ui/menubar.glade:8
#: data/ui/status-icon-menu.glade:6 src/ui/gv-station-context-menu.c:35
#: src/ui/gv-station-dialog.c:428
msgid "Add Station"
msgstr ""

#: data/ui/app-menu.glade:13 data/ui/menubar.glade:15
#: data/ui/status-icon-menu.glade:12 src/ui/gv-prefs-window.c:633
msgid "Preferences"
msgstr ""

#: data/ui/app-menu.glade:19 data/ui/menubar.glade:36
#: data/ui/status-icon-menu.glade:18
msgid "Online Help"
msgstr ""

#: data/ui/app-menu.glade:24 data/ui/menubar.glade:43
#: data/ui/status-icon-menu.glade:22
msgid "About"
msgstr ""

#: data/ui/app-menu.glade:28 data/ui/menubar.glade:21
msgid "Close UI"
msgstr ""

#: data/ui/app-menu.glade:33 data/ui/menubar.glade:26
#: data/ui/status-icon-menu.glade:26
msgid "Quit"
msgstr ""

#: data/ui/main-window.glade:47 src/ui/gv-main-window.c:351
msgid "No station selected"
msgstr ""

#: data/ui/main-window.glade:63 src/ui/gv-main-window.c:374
msgid "Stopped"
msgstr ""

#: data/ui/menubar.glade:5
msgid "Menu"
msgstr ""

#: data/ui/menubar.glade:33
msgid "Help"
msgstr ""

#: data/ui/prefs-window.glade:35
msgid "Autoplay on Startup"
msgstr ""

#: data/ui/prefs-window.glade:48
msgid "Custom Output Pipeline"
msgstr ""

#: data/ui/prefs-window.glade:79
msgid "Apply"
msgstr ""

#: data/ui/prefs-window.glade:102
msgid "Playback"
msgstr ""

#: data/ui/prefs-window.glade:138
msgid "Prevent sleep while playing"
msgstr ""

#: data/ui/prefs-window.glade:151
msgid "System"
msgstr ""

#: data/ui/prefs-window.glade:197
msgid "Native D-Bus Server"
msgstr ""

#: data/ui/prefs-window.glade:209
msgid "MPRIS2 D-Bus Server"
msgstr ""

#: data/ui/prefs-window.glade:222
msgid "D-Bus"
msgstr ""

#: data/ui/prefs-window.glade:238
msgid "Misc"
msgstr ""

#: data/ui/prefs-window.glade:262
msgid "Autoset Window Height"
msgstr ""

#: data/ui/prefs-window.glade:278
msgid "Theme Variant"
msgstr ""

#: data/ui/prefs-window.glade:291
msgid "System Default"
msgstr ""

#: data/ui/prefs-window.glade:292
msgid "Prefer Dark"
msgstr ""

#: data/ui/prefs-window.glade:293
msgid "Prefer Light"
msgstr ""

#: data/ui/prefs-window.glade:310
msgid "Window"
msgstr ""

#: data/ui/prefs-window.glade:336
msgid "Send Notifications"
msgstr ""

#: data/ui/prefs-window.glade:359
msgid "Notifications"
msgstr ""

#: data/ui/prefs-window.glade:394
msgid "Console Output"
msgstr ""

#: data/ui/prefs-window.glade:407
msgid "Console"
msgstr ""

#: data/ui/prefs-window.glade:426
msgid "Display"
msgstr ""

#: data/ui/prefs-window.glade:464
msgid "Multimedia Hotkeys"
msgstr ""

#: data/ui/prefs-window.glade:477
msgid "Keyboard"
msgstr ""

#: data/ui/prefs-window.glade:503
msgid "Middle Click"
msgstr ""

#: data/ui/prefs-window.glade:516
msgid "Toggle Play/Pause"
msgstr ""

#: data/ui/prefs-window.glade:517
msgid "Mute"
msgstr ""

#: data/ui/prefs-window.glade:530
msgid "Scrolling"
msgstr ""

#: data/ui/prefs-window.glade:543
msgid "Change Station"
msgstr ""

#: data/ui/prefs-window.glade:544
msgid "Change Volume"
msgstr ""

#: data/ui/prefs-window.glade:558
msgid "Mouse (Status Icon Mode)"
msgstr ""

#: data/ui/prefs-window.glade:577
msgid "Controls"
msgstr ""

#: data/ui/prefs-window.glade:598
msgid "Close"
msgstr ""

#: data/ui/station-dialog.glade:12 src/ui/gv-main-window.c:262
msgid "Name"
msgstr ""

#: data/ui/station-dialog.glade:34 src/ui/gv-main-window.c:263
msgid "URI"
msgstr ""

#: src/core/gv-engine.c:267 src/core/gv-station-list.c:1112
#, c-format
msgid "%s: %s"
msgstr ""

#: src/core/gv-engine.c:268
msgid "Failed to parse pipeline description"
msgstr ""

#: src/core/gv-player.c:819
#, c-format
msgid "'%s' is neither a known station or a valid URI"
msgstr ""

#: src/core/gv-station-list.c:1113
msgid "Failed to save station list"
msgstr ""

#: src/ui/gv-main-window.c:260
msgid "Station Information"
msgstr ""

#: src/ui/gv-main-window.c:274
msgid "User-agent"
msgstr ""

#: src/ui/gv-main-window.c:278
msgid "Bitrate"
msgstr ""

#: src/ui/gv-main-window.c:291
msgid "Metadata"
msgstr ""

#: src/ui/gv-main-window.c:293
msgid "Artist"
msgstr ""

#: src/ui/gv-main-window.c:294
msgid "Title"
msgstr ""

#: src/ui/gv-main-window.c:295
msgid "Album"
msgstr ""

#: src/ui/gv-main-window.c:296
msgid "Genre"
msgstr ""

#: src/ui/gv-main-window.c:297
msgid "Year"
msgstr ""

#: src/ui/gv-main-window.c:298
msgid "Comment"
msgstr ""

#: src/ui/gv-main-window.c:364 src/ui/gv-main-window.c:392
msgid "Playing"
msgstr ""

#: src/ui/gv-main-window.c:367
msgid "Connecting…"
msgstr ""

#: src/ui/gv-main-window.c:370
msgid "Buffering…"
msgstr ""

#: src/ui/gv-prefs-window.c:282
msgid "Feature disabled at compile-time."
msgstr ""

#.
#. * Setup settings and features.
#. * These function calls create a binding between a gtk widget and
#. * an internal object, initializes the widget value, and set the
#. * widgets tooltips (label + setting).
#.
#. Misc
#: src/ui/gv-prefs-window.c:428
msgid "Whether to start playback automatically on startup."
msgstr ""

#: src/ui/gv-prefs-window.c:434
msgid "Whether to use a custom output pipeline."
msgstr ""

#: src/ui/gv-prefs-window.c:440
msgid ""
"The GStreamer output pipeline used for playback. Refer to theonline "
"documentation for examples."
msgstr ""

#: src/ui/gv-prefs-window.c:456
msgid "Prevent the system from going to sleep while playing."
msgstr ""

#: src/ui/gv-prefs-window.c:461
msgid "Enable the native D-Bus server (needed for the command-line interface)."
msgstr ""

#: src/ui/gv-prefs-window.c:467
msgid "Enable the MPRIS2 D-Bus server."
msgstr ""

#. Display
#: src/ui/gv-prefs-window.c:473
msgid "Prefer a different variant of the theme (if available)."
msgstr ""

#: src/ui/gv-prefs-window.c:480
msgid ""
"Automatically adjust the window height when a station is added or removed."
msgstr ""

#: src/ui/gv-prefs-window.c:487
msgid "Setting not available in status icon mode."
msgstr ""

#: src/ui/gv-prefs-window.c:491
msgid "Show notification when the status changes."
msgstr ""

#: src/ui/gv-prefs-window.c:496
msgid "Display information on the standard output."
msgstr ""

#. Controls
#: src/ui/gv-prefs-window.c:502
msgid "Bind mutimedia keys (play/pause/stop/previous/next)."
msgstr ""

#: src/ui/gv-prefs-window.c:508
msgid "Action triggered by a middle click on the status icon."
msgstr ""

#: src/ui/gv-prefs-window.c:514
msgid "Action triggered by mouse-scrolling on the status icon."
msgstr ""

#: src/ui/gv-prefs-window.c:520
msgid "Setting only available in status icon mode."
msgstr ""

#: src/ui/gv-station-context-menu.c:36
msgid "Remove Station"
msgstr ""

#: src/ui/gv-station-context-menu.c:37 src/ui/gv-station-dialog.c:428
msgid "Edit Station"
msgstr ""

#: src/ui/gv-station-dialog.c:284
msgid "Cancel"
msgstr ""

#: src/ui/gv-station-dialog.c:285
msgid "Save"
msgstr ""

#: src/ui/gv-status-icon.c:130
msgid "stopped"
msgstr ""

#: src/ui/gv-status-icon.c:133
msgid "connecting"
msgstr ""

#: src/ui/gv-status-icon.c:136
msgid "buffering"
msgstr ""

#: src/ui/gv-status-icon.c:139
msgid "playing"
msgstr ""

#: src/ui/gv-status-icon.c:142
msgid "unknown state"
msgstr ""

#: src/ui/gv-status-icon.c:149
msgid "muted"
msgstr ""

#: src/ui/gv-status-icon.c:153
msgid "vol."
msgstr ""

#: src/ui/gv-status-icon.c:160
msgid "No station"
msgstr ""

#: src/ui/gv-status-icon.c:167
msgid "No metadata"
msgstr ""

#: src/feat/gv-hotkeys.c:145
#, c-format
msgid ""
"%s:\n"
"%s"
msgstr ""

#: src/feat/gv-hotkeys.c:146
msgid "Failed to bind the following keys"
msgstr ""

#: src/feat/gv-inhibitor.c:111
msgid "Failed to inhibit system sleep"
msgstr ""

#: src/feat/gv-notifications.c:59
#, c-format
msgid "Playing %s"
msgstr ""

#: src/feat/gv-notifications.c:62
#, c-format
msgid "Playing <%s>"
msgstr ""

#: src/feat/gv-notifications.c:65
msgid "Playing Station"
msgstr ""

#: src/feat/gv-notifications.c:103
msgid "(Unknown title)"
msgstr ""

#: src/feat/gv-notifications.c:110
msgid "New Track"
msgstr ""

#: src/feat/gv-notifications.c:122
msgid "Error"
msgstr ""
