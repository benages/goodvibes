My Autotools Bookmarks

- [Autotools Mythbuster](https://autotools.io/index.html)
- Russ Allbery's [Build System Coding Style](https://www.eyrie.org/~eagle/notes/style/build.html)
- Gentoos's [Basics of Autotools](https://devmanual.gentoo.org/general-concepts/autotools/)
