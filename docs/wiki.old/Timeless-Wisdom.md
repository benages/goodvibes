Some timeless wisdom that needs to be repeated again and again.



> Smart data structures and dumb code works a lot better than the other way around.

Eric S. Raymond - The Cathedral and the Bazaar

> It is simple to make things complex, but complex to make things simple.

Meyer's 3rd Law

> It seems that perfection is attained not when there is nothing more to add, but when there is nothing more to remove.

Antoine de Saint-Exupéry - Wind, Sand and Stars - 1939
