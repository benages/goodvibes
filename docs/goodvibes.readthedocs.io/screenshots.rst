Screenshots
===========

Modern Desktops
---------------

Nowadays, desktop environments usually provide a media player applet,
integrated somewhere in the desktop user interface. This applet provides basic
control over media players that implement the `MPRIS2
<https://specifications.freedesktop.org/mpris-spec/latest/>`_ specification.

These screenshots show both Goodvibes and the media player applet from
different desktop environments.

.. figure:: images/screenshot-2017-01-gnome.png
   :scale: 100%
   :align: center

   **GNOME** (Ubuntu 16.04 LTS) with the Media Player Indicator extension

.. figure:: images/screenshot-2017-01-ubuntu.png
   :scale: 100%
   :align: center

   **Unity** (Ubuntu 16.04 LTS)

.. figure:: images/screenshot-2017-01-pantheon.png
   :scale: 100%
   :align: center

   **Pantheon** (Elementary OS 0.4)

Old-style Desktops
------------------

It's possible to launch Goodvibes with the ``--status-icon`` option. In this
mode, Goodvibes will not display a main window, but will instead add an icon
to your notification area (also called system tray).

This is a kind of legacy feature, and it will be removed from Goodvibes at some
point in the future. This is not supported by the toolkit (GTK+) anymore, and
this is also disappearing from various desktop environments (GNOME and Ubuntu
at least).

Anyway, here's how Goodvibes looks like in *Status Icon* mode.

.. figure:: images/screenshot-2017-01-mate.png
   :scale: 100%
   :align: center

   **MATE** (Linux Mint 18.1)

.. figure:: images/screenshot-2017-01-openbox-tint2.png
   :scale: 100%
   :align: center

   **Openbox/Tint2** (Debian Stretch)
