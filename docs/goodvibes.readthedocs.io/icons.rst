.. |arch-logo| raw:: html

   <i class="icon-archlinux"></i>

.. |debian-logo| raw:: html

   <i class="icon-debian"></i>

.. |source-logo| raw:: html

   <i class="fa fa-file"></i>

.. |ubuntu-logo| raw:: html

   <i class="icon-ubuntu"></i>
