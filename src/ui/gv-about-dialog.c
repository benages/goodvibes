/*
 * Goodvibes Radio Player
 *
 * Copyright (C) 2015-2018 Arnaud Rebillout
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <gtk/gtk.h>

#include "framework/gv-framework.h"

static const gchar *authors[] = {
	GV_AUTHOR_NAME " <" GV_AUTHOR_EMAIL ">",
	NULL
};

static const gchar *artists[] = {
	"Lahminèwski Lab http://lahminewski-lab.net",
	NULL
};

static const gchar *translators =
	"Weblate https://hosted.weblate.org/projects/goodvibes/translations\n" \
	"Lukáš Linhart <lukighostmail@gmail.com> - Czech (cs)\n" \
	"Michal Čihař <michal@cihar.com> - Czech (cs)\n" \
	"Andreas Kleinert <Andy.Kleinert@gmail.com> - German (de)\n" \
	"Vinz <vinz@vinzv.de> - German (de)\n" \
	"Wolf Berg <ddf051.wolf@googlemail.com> - German (de)\n" \
	"Étienne Deparis <etienne@depar.is> - French (fr)\n" \
	"Allan Nordhøy <epost@anotheragency.no> - Norwegian Bokmål (nb_NO)\n" \
	"Heimen Stoffels <vistausss@outlook.com> - Dutch (nl)\n" \
	"Алексей Выскубов <viskubov@gmail.com> - Russian (ru)";

void
gv_show_about_dialog(GtkWindow *parent, const gchar *audio_backend_string,
                     const gchar *ui_toolkit_string)
{
	// WISHED "license-type" shouldn't be hardcoded

	gchar *comments;

	comments = g_strdup_printf("Audio Backend: %s\n"
	                           "GUI Toolkit: %s",
	                           audio_backend_string,
	                           ui_toolkit_string);

	gtk_show_about_dialog(parent,
	                      "artists", artists,
	                      "authors", authors,
	                      "comments", comments,
	                      "copyright", GV_COPYRIGHT " " GV_AUTHOR_NAME,
	                      "license-type", GTK_LICENSE_GPL_3_0,
	                      "logo-icon-name", PACKAGE_NAME,
			      "translator-credits", translators,
	                      "version", PACKAGE_VERSION,
	                      "website", GV_HOMEPAGE,
	                      NULL);

	g_free(comments);
}
